/*
 * Copyright 2020 Georgia Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdio>
#include <stdint.h>
#include <vector>

#include "tclap/CmdLine.h"
#include "utils/tclap_utils.hpp"

#include "utils/benchmark_utils.hpp"
#include "utils/mem_utils.hpp"

#include "mxx/env.hpp"
#include "mxx/comm.hpp"
#include "mxx/reduction.hpp"

int main(int argc, char** argv) {

  BL_BENCH_INIT(bm);

  BL_BENCH_START(bm);

  mxx::env e(argc, argv);
  mxx::comm comm;

  int comm_size = comm.size();
  int comm_rank = comm.rank();

  if (comm_size <= 1) {
    std::cout << "ERROR: this benchmark is meant to be run with more than 1 MPI processes." << std::endl;
    return 1;
  }

  BL_BENCH_END(bm, "mpi_init", comm_size);
  
  comm.barrier();

  if (comm_rank == 0) {
    std::cout << "MPI Init completed." << std::endl;
  }

  comm.barrier();
  
  BL_BENCH_START(bm);
  size_t message_size = static_cast<size_t>(1) << 16;
  size_t warmup = static_cast<size_t>(16);


  // Wrap everything in a try block.  Do this every time,
  // because exceptions will be thrown for problems.
  try {

    // Define the command line object, and insert a message
    // that describes the program. The "Command description message"
    // is printed last in the help text. The second argument is the
    // delimiter (usually space) and the last one is the version number.
    // The CmdLine object parses the argv array based on the Arg objects
    // that it contains.
    TCLAP::CmdLine cmd("allreduce microbenchmark", ' ', "0.1");

    // Define a value argument and add it to the command line.
    // A value arg defines a flag and a type of value that it expects,
    // such as "-n Bishop".
    TCLAP::ValueArg<size_t> msizeArg("m",
                                 "message-size", "message size constant. Default=256MB.",
                                 false, message_size, "size_t", cmd);    
    TCLAP::ValueArg<size_t> warmupArg("w",
                                 "warmup-iters", "warm up iterations. Default=16.",
                                 false, warmup, "size_t", cmd);
                                 
    // Parse the argv array.
    cmd.parse( argc, argv );

    message_size = msizeArg.getValue(); 
    warmup = warmupArg.getValue(); 

  if (message_size < 1) message_size = 1;  // at least 1.
  if (warmup < 1) warmup = 16;

  } catch (TCLAP::ArgException &e)  // catch any exceptions
  {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    exit(-1);
  }
  if (comm_rank == 0) std::cout << "BENCHMARKING MPI_Allreduce with " << message_size << " elements " << std::endl;
  if (comm_rank == 0) std::cout << "BENCHMARKING MPI_Allreduce warmup " << warmup << " iters " << std::endl;

  BL_BENCH_END(bm, "parse_input", comm_size);


  BL_BENCH_COLLECTIVE_START(bm, "alloc", comm);
  //   mxx::datatype dt = mxx::get_datatype<size_t>();
  
  size_t * src = ::utils::mem::aligned_alloc<size_t>(message_size);
  size_t * dest = ::utils::mem::aligned_alloc<size_t>(message_size);

  BL_BENCH_END(bm, "alloc", comm_size);

  BL_BENCH_COLLECTIVE_START(bm, "allreduce warmup", comm);
  for (size_t i = 0; i < warmup; ++i) {
    MPI_Allreduce(src, dest, 1, MPI_UNSIGNED_LONG_LONG, MPI_BOR, comm);
  }
  BL_BENCH_END(bm, "allreduce_warmup", message_size);

  BL_BENCH_COLLECTIVE_START(bm, "allreduce", comm);
  MPI_Allreduce(src, dest, message_size, MPI_UNSIGNED_LONG_LONG, MPI_BOR, comm);
  BL_BENCH_END(bm, "allreduce", message_size);

  BL_BENCH_COLLECTIVE_START(bm, "allreduce_inplace", comm);
  MPI_Allreduce(MPI_IN_PLACE, dest, message_size, MPI_UNSIGNED_LONG_LONG, MPI_BOR, comm);
  BL_BENCH_END(bm, "allreduce_inplace", message_size);


  BL_BENCH_COLLECTIVE_START(bm, "cleanup", comm);

  ::utils::mem::aligned_free(src);
  ::utils::mem::aligned_free(dest);

  BL_BENCH_END(bm, "cleanup", message_size * comm_size);



  BL_BENCH_REPORT_MPI_NAMED(bm, "benchmark", comm);

  comm.barrier();
}
