/*
 * Copyright 2015 Georgia Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdio>
#include <stdint.h>
#include <vector>

#include "tclap/CmdLine.h"
#include "utils/tclap_utils.hpp"


#include "utils/benchmark_utils.hpp"
#include "utils/mem_utils.hpp"

#include "mxx/env.hpp"
#include "mxx/comm.hpp"

int main(int argc, char** argv) {

  BL_BENCH_INIT(bm);

  BL_BENCH_START(bm);

  mxx::env e(argc, argv);
  mxx::comm comm;

  int comm_size = comm.size();
  int comm_rank = comm.rank();

  if (comm_size <= 1) {
    std::cout << "ERROR: this benchmark is meant to be run with more than 1 MPI processes." << std::endl;
    return 1;
  }

  BL_BENCH_END(bm, "mpi_init", comm_size);
  
  comm.barrier();

  if (comm_rank == 0) {
    std::cout << "MPI Init completed." << std::endl;
  }

  comm.barrier();
  
  BL_BENCH_START(bm);
  size_t message_size = static_cast<size_t>(1) << 28;
  bool strong = false;


  // Wrap everything in a try block.  Do this every time,
  // because exceptions will be thrown for problems.
  try {

    // Define the command line object, and insert a message
    // that describes the program. The "Command description message"
    // is printed last in the help text. The second argument is the
    // delimiter (usually space) and the last one is the version number.
    // The CmdLine object parses the argv array based on the Arg objects
    // that it contains.
    TCLAP::CmdLine cmd("memory microbenchmark", ' ', "0.1");

    // Define a value argument and add it to the command line.
    // A value arg defines a flag and a type of value that it expects,
    // such as "-n Bishop".

    // output algo 7 and 8 are not working.
    //
    // Let m denote a constant message size.
    // for strong scaling, specify the constant total message size m = N.
    // for weak scaling, specify the constant per-processor message size m = N/p 
    // for weak scaling measurement of MPI performance, we want processor-pair message size
    //   to be constant so that the same MPI algorithm is used as P is scaled up,
    //   supply P*(constant proc pair message size m= N/pp).
    TCLAP::ValueArg<size_t> msizeArg("m",
                                 "message-size", "message size constant. Strong->TOTAL, Default=256MB.  Weak->Per_proc.",
                                 false, message_size, "size_t", cmd);
    TCLAP::SwitchArg scalingArg("s",
                                 "strong", "strong scaling? Default=false",
                                 cmd, false);
                                 

    // Parse the argv array.
    cmd.parse( argc, argv );

    strong = scalingArg.getValue();
    message_size = msizeArg.getValue() / comm_size;  // if weak scaling, each rank is assigned this amount of data.
    if (strong) message_size /= comm_size;    // if strong scaling, each pairing splits the per-rank assigned data.

    if (message_size < 1) message_size = 1;  // at least 1.

  } catch (TCLAP::ArgException &e)  // catch any exceptions
  {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    exit(-1);
  }
  if (comm_rank == 0) std::cout << "BENCHMARKING MPI_Alltoall with " << message_size << " elements per rank pair, " << std::endl;
  if (comm_rank == 0) std::cout << (strong ? "strong" : "weak") << " scaling, comm size = " << comm_size << std::endl << std::flush;

  BL_BENCH_END(bm, "parse_input", comm_size);


  BL_BENCH_COLLECTIVE_START(bm, "alloc", comm);
  mxx::datatype dt = mxx::get_datatype<size_t>();
  
  size_t * src = ::utils::mem::aligned_alloc<size_t>(message_size * comm_size);
  size_t * dest = ::utils::mem::aligned_alloc<size_t>(message_size * comm_size);

  BL_BENCH_END(bm, "alloc", comm_size);

  BL_BENCH_COLLECTIVE_START(bm, "warmup", comm);
  MPI_Alltoall(src, 1, dt.type(), dest, 1, dt.type(), comm);
  BL_BENCH_END(bm, "warmup1", comm_size);

  BL_BENCH_COLLECTIVE_START(bm, "warmup", comm);
  MPI_Alltoall(src, message_size, dt.type(), dest, message_size, dt.type(), comm);
  BL_BENCH_END(bm, "warmup", message_size*comm_size);

  BL_BENCH_COLLECTIVE_START(bm, "alltoall", comm);
  MPI_Alltoall(src, message_size, dt.type(), dest, message_size, dt.type(), comm);
  BL_BENCH_END(bm, "alltoall", message_size*comm_size);

  BL_BENCH_COLLECTIVE_START(bm, "alltoall_inplace", comm);
  MPI_Alltoall(MPI_IN_PLACE, message_size, dt.type(), dest, message_size, dt.type(), comm);
  BL_BENCH_END(bm, "alltoall_inplace", message_size*comm_size);

  BL_BENCH_COLLECTIVE_START(bm, "cleanup", comm);

  ::utils::mem::aligned_free(src);
  ::utils::mem::aligned_free(dest);

  BL_BENCH_END(bm, "cleanup", message_size * comm_size);

  BL_BENCH_REPORT_MPI_NAMED(bm, "benchmark", comm);

  comm.barrier();
}
