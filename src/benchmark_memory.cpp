/*
 * Copyright 2015 Georgia Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "bliss-config.hpp"

#include <cstdio>
#include <stdint.h>
#include <vector>
#include <ctime>
#include <stdlib.h>
#include <algorithm>
#include <unistd.h> // usleep
#include <future>  // future, async, etc.
#include <random>

#include "tclap/CmdLine.h"
#include "utils/tclap_utils.hpp"

#include "utils/benchmark_utils.hpp"
#include "utils/mem_utils.hpp"

#include "omp.h"


// generate about 1000 elements, with +/- 5% variability
std::vector<size_t> generateCounts(size_t const & per_pair_elem_count, float const & window, int const & threads){
  srand((static_cast<uint32_t>(1) << 19) - 1);

  size_t var = per_pair_elem_count * window;

  std::vector<size_t> counts;
  counts.reserve(threads);

  for (int i = 0; i < threads; ++i) {
    counts.emplace_back(per_pair_elem_count - (var / 2) + (rand() % var));
  }

  return counts;
}

struct sim_work {
    size_t cycles;

    sim_work(size_t const & c = 20) : cycles(c) {}

    template <typename T>
    T operator()(T const & x) {
      T y = x;
      for (size_t i = 0; i < cycles; ++i) {
        y += i;
      }
      return y;
    }
};



int main(int argc, char** argv) {


  BL_BENCH_INIT(bm);

  BL_BENCH_START(bm);

  int nthreads = omp_get_max_threads();
  omp_set_dynamic(0);

  if (nthreads <= 1) {
    std::cout << "ERROR: this benchmark is meant to be run with more than 1 OMP threads." << std::endl;
    return 1;
	}

  #pragma omp parallel
  {
	  //if (omp_get_thread_num() == 0) printf("thread %d of %d ready\n", omp_get_thread_num(), omp_get_num_threads());
    printf(".");
  }
  BL_BENCH_END(bm, "omp_init", nthreads);


  BL_BENCH_START(bm);
  int work_cpe = 100;
  int message_size = static_cast<int>(1) << 28;
  float variance = 0.03;
  bool strong = false;


  // Wrap everything in a try block.  Do this every time,
  // because exceptions will be thrown for problems.
  try {

    // Define the command line object, and insert a message
    // that describes the program. The "Command description message"
    // is printed last in the help text. The second argument is the
    // delimiter (usually space) and the last one is the version number.
    // The CmdLine object parses the argv array based on the Arg objects
    // that it contains.
    TCLAP::CmdLine cmd("memory microbenchmark", ' ', "0.1");

    // Define a value argument and add it to the command line.
    // A value arg defines a flag and a type of value that it expects,
    // such as "-n Bishop".

    // output algo 7 and 8 are not working.
    TCLAP::ValueArg<int> msizeArg("m",
                                 "message-size", "total message size in bytes Default=8KB",
                                 false, message_size, "int", cmd);
    TCLAP::ValueArg<int> cpeArg("c",
                                 "work-cpe", "Cycle Per Element for computation, for simulating comm-compute overlap.  Default=20",
                                 false, work_cpe, "int", cmd);
    TCLAP::ValueArg<float> varArg("v",
                                     "variance", "range in which the number of elements for each rank-pair can vary, as percent of average.  Default=0.03",
                                     false, variance, "float", cmd);
    TCLAP::ValueArg<int> threadArg("t",
                                 "threads", "number of threads Default=MAX",
                                 false, nthreads, "int", cmd);
    TCLAP::SwitchArg scalingArg("s",
                                 "strong", "strong scaling? Default=false",
                                 cmd, false);
                                 

    // Parse the argv array.
    cmd.parse( argc, argv );

    message_size = msizeArg.getValue();
    work_cpe = cpeArg.getValue();
    variance = varArg.getValue();
    nthreads = threadArg.getValue();
    strong = scalingArg.getValue();

  } catch (TCLAP::ArgException &e)  // catch any exceptions
  {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    exit(-1);
  }


  sim_work sw(work_cpe);

  omp_set_num_threads(nthreads);   // override default.

  BL_BENCH_END(bm, "parse_input", 0);


  if (nthreads > omp_get_max_threads()) {
    throw std::invalid_argument("specified threads exceed number allowed.");
  }


  std::cout << "BENCHMARKING memory access with " << argv[0] << " threads = " << nthreads << std::endl << std::flush;

  /////// CENTRAL: a single global array, partitioned to different threads.  each thread accessing only its own partition.
  BL_BENCH_START(bm);
  size_t per_thread_pair_count = message_size / sizeof(size_t);  
  if (strong) per_thread_pair_count /= nthreads;  // per thread_pair_count * nthreads -> weak scaling.  to make it strong scaling, divide by nthreads again.
  std::vector<size_t> send_counts = generateCounts(per_thread_pair_count, variance, nthreads);
  std::vector<size_t> send_displs(nthreads, 0);

  // ex scan
  std::partial_sum(send_counts.begin(), send_counts.begin() + send_counts.size() - 1, send_displs.begin() + 1);
  size_t src_size = send_displs.back() + send_counts.back();
  BL_BENCH_END(bm, "compute_sizes", src_size);

  BL_BENCH_START(bm);
  size_t * src = ::utils::mem::aligned_alloc<size_t>(src_size);
  std::cout << "  total size_t count: " << src_size << std::endl;
  #pragma omp parallel
  {
    int tid = omp_get_thread_num();

    std::random_device rd;
    std::mt19937_64 mt(rd()); 
    std::uniform_int_distribution<size_t> distr;

    size_t mx = send_displs[tid] + send_counts[tid];
    for (size_t i = send_displs[tid]; i < mx; ++i) {
      src[i] = distr(mt);
    }
  }
  BL_BENCH_END(bm, "init_central_src", src_size);

  BL_BENCH_START(bm);
  size_t * rand_ids = ::utils::mem::aligned_alloc<size_t>(src_size);
  #pragma omp parallel
  {
    int tid = omp_get_thread_num();

    size_t * rids = rand_ids + send_displs[tid];

    std::iota(rids , rids + send_counts[tid], send_displs[tid]);
    std::shuffle(rids, rids + send_counts[tid], std::mt19937_64{std::random_device{}()} );
  }
  BL_BENCH_END(bm, "init_central_rand", src_size);

  ///////////////// -----------  seq compute
  BL_BENCH_START(bm);
  size_t * dest = ::utils::mem::aligned_alloc<size_t>(src_size);
  BL_BENCH_END(bm, "init_central_dest", src_size);


  {
    ///// iterate and computer linearly in the central array
    BL_BENCH_START(bm);
    #pragma omp parallel
    {
      int tid = omp_get_thread_num();
      if (work_cpe > 0)
        std::transform(src + send_displs[tid], src + send_displs[tid] + send_counts[tid],
                       dest + send_displs[tid], sw);
      else 
        std::copy(src + send_displs[tid], src + send_displs[tid] + send_counts[tid],
                       dest + send_displs[tid]);
    }
    BL_BENCH_END(bm, "linear_central_compute", src_size);


    ///////////////// -----------  rand compute, thread local

    ///// iterate and computer with random walk (rand_ids) in the central array
    BL_BENCH_START(bm);
    #pragma omp parallel
    {
      int tid = omp_get_thread_num();
      size_t id;
      size_t* rids = rand_ids + send_displs[tid];
      if (work_cpe > 0)
        for (size_t i = 0; i < send_counts[tid]; ++i, ++rids) {
          id = *rids;
          dest[id] = sw(src[id]);
        }
      else 
        for (size_t i = 0; i < send_counts[tid]; ++i, ++rids) {
          id = *rids;
          dest[id] = src[id];
        }
    }
    BL_BENCH_END(bm, "rand_central_compute", src_size);

  }



  {
  /////// LOCAL: thread local arrays.

    BL_BENCH_START(bm);
    std::vector<size_t *> srcs(nthreads);


    BL_BENCH_START(bm);
    #pragma omp parallel
    {
      int tid = omp_get_thread_num();
      srcs[tid] = ::utils::mem::aligned_alloc<size_t>(send_counts[tid]);

      memcpy(srcs[tid], src + send_displs[tid], send_counts[tid] * sizeof(size_t));
    }
    BL_BENCH_END(bm, "init_local_src", src_size);


    BL_BENCH_START(bm);
    std::vector<size_t *> rand_idss(nthreads);
    #pragma omp parallel
    {
      int tid = omp_get_thread_num();
      //size_t offset = send_displs[tid];
      size_t cnts = send_counts[tid];

      rand_idss[tid] = ::utils::mem::aligned_alloc<size_t>(cnts);

      // memcpy(rand_idss[tid], rand_ids + offset, cnts * sizeof(size_t));

      // // need to subtract the offsets

      // std::transform(rand_ids + offset, rand_ids + offset + cnts,
      //   rand_idss[tid], [&offset](size_t const & x){
      //     return x - offset;
      //   });
      std::iota(rand_idss[tid] , rand_idss[tid] + cnts, static_cast<size_t>(0));
      std::shuffle(rand_idss[tid], rand_idss[tid] + cnts, std::mt19937_64{std::random_device{}()} );
    }
    BL_BENCH_END(bm, "init_local_rand", src_size);


    BL_BENCH_START(bm);
    std::vector<size_t *> dests(nthreads);
    #pragma omp parallel
    {
      int tid = omp_get_thread_num();
      dests[tid] = ::utils::mem::aligned_alloc<size_t>(send_counts[tid]);
    }
    BL_BENCH_END(bm, "init_local_dest", src_size);

    ///////////////// -----------  seq compute
    // sequential walk inside thread local array
    BL_BENCH_START(bm);
    #pragma omp parallel
    {
      int tid = omp_get_thread_num();
      if (work_cpe > 0)
        std::transform(srcs[tid], srcs[tid] + send_counts[tid],
                      dests[tid], sw);
      else
        std::copy(srcs[tid], srcs[tid] + send_counts[tid],
                        dests[tid]);
    }
    BL_BENCH_END(bm, "linear_local_compute", src_size);


   ///////////////// -----------  rand compute, thread local
    ///// random walk inside thread local array
   BL_BENCH_START(bm);
   #pragma omp parallel
   {
     int tid = omp_get_thread_num();
     size_t id;
     size_t* rids = rand_idss[tid];
     if (work_cpe > 0)
       for (size_t i = 0; i < send_counts[tid]; ++i, ++rids) {
         id = *rids;
         dests[tid][id] = sw(srcs[tid][id]);
       }
     else
       for (size_t i = 0; i < send_counts[tid]; ++i, ++rids) {
         id = *rids;
         dests[tid][id] = srcs[tid][id];
       }
   }
   BL_BENCH_END(bm, "rand_local_compute", src_size);





    BL_BENCH_START(bm);
    #pragma omp parallel
    {
      int tid = omp_get_thread_num();
      ::utils::mem::aligned_free(srcs[tid]);
      ::utils::mem::aligned_free(dests[tid]);
      ::utils::mem::aligned_free(rand_idss[tid]);
    }
    BL_BENCH_END(bm, "local_cleanup", src_size);

  }

  
  {
    ///////////////// -----------  rand compute, global

    // central array, random walk in entire array by all threads (not confined to partition)

    BL_BENCH_START(bm);
	#pragma omp parallel
	{
	  int tid = omp_get_thread_num();
	  size_t * rids = rand_ids + send_displs[tid];

	  std::iota(rids , rids + send_counts[tid], send_displs[tid]);
	}
    std::shuffle(rand_ids, rand_ids + src_size, std::mt19937_64{std::random_device{}()} );
    BL_BENCH_END(bm, "init_global_rand", src_size);


    BL_BENCH_START(bm);
    #pragma omp parallel
    {
      int tid = omp_get_thread_num();
      size_t id;
      size_t* rids = rand_ids + send_displs[tid];
      if (work_cpe > 0)
        for (size_t i = 0; i < send_counts[tid]; ++i, ++rids) {
          id = *rids;
          dest[id] = sw(src[id]);
        }
      else
        for (size_t i = 0; i < send_counts[tid]; ++i, ++rids) {
          id = *rids;
          dest[id] = src[id];
        }
    }
    BL_BENCH_END(bm, "rand_global_compute", src_size);


  }

  BL_BENCH_START(bm);
  ::utils::mem::aligned_free(src);
  ::utils::mem::aligned_free(rand_ids);
  ::utils::mem::aligned_free(dest);
  BL_BENCH_END(bm, "central_cleanup", src_size);

  BL_BENCH_REPORT_NAMED(bm, "benchmark");

}
