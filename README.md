# Benchmark for OpenMP memory access and MPI

This project focus performance benchmarking for the specific use cases where data is moved/accessed and then computation is applied to the data, with results potentially written back to memory.  For in memory access, the benchmark aims to measure memory access scaling as number of threads increases.   For MPI, we focus on Alltoall and Alltoallv.  The benchmark also reimplements the MPI Alltoall and Alltoallv algorithms that use point-to-point primitives (e.g. for medium to large messages) and implements a specialization where computation is overlapped with non-blocking version of the point-to-point primitives.

# Setup

## Getting the source

After downloading the source, the dependent libraries must be retrieved by invoking the following in the source directory.
```sh
git submodule init
git submodule update
```

## Compilation Prerequisites

The project has been tested with gcc 4.9 through 5.3, and 7.3.  It requires OpenMP and an MPI distribution.  OpenMPI 1.8, MVAPICH2 2.1 and 2.3b, and Intel MPI 5.1.1 have been tested.  We also require gnu make, cmake and optionally ccmake.

## Configuring the Project

The STRONGLY recommended practice is to compile outside of the source download.  The following commands configures a binary installation using cmake.  Assume BUILD_DIR is the directory where the project binaries are to be built, and SRC_DIR is the source directory

```sh
mkdir BUILD_DIR
cd BUILD_DIR
cmake SRC_DIR
```

At this point, the project has been configured with the default configurations.  Alternatively, "ccmake SRC_DIR" can be used to configure the project via an interactive UI.

## Compiling
Standard "make" works.

```sh
make
```

## Generated Binaries

The binaries generated are meant to be run on a cluster with a job scheduler. A set of example job scripts have been provided.  For completeness, the list of binaries is provided below.  Running each with "--help" will show the commandline parameter list.

```
benchmark_a2a_simple
benchmark_a2av
benchmark_mem
```


# Running

Several example scripts have been provided in the "script" directory.

PBS scripts (those with file suffix ".pbs") test scaling with varying cores per node.  The "submit_scaling_runs.sh" script submits multiple jobs to test scaling as the number of nodes are increased.  The combinations of these scripts allows a benchmark result matrix to be constructed, with one dimension being node count and the second dimension being the process-per-node count.

## submit_scaling_runs.sh

```
./submit_scaling_runs.sh pbs_script [existing_job_id]
```
This script submits a sequence of pbs jobs with dependency set to "afterany", so that no two jobs are run concurrently.  The jobs request 1, 2, 3, 4, 7, 8, 14, 16, 28, 32, 56, and 64.  Larger node count jobs are requested later to minimize blocking by downed or unresponsive nodes.

The "existing_job_id" parameter is optional and allows a sequence of jobs to depend on some other previously submitted job.

The "submit_scaling_runs.sh" reserves all cores on each requested node.  The process-per-node, or core-per-node, should be configured in the submitted "pbs_script".  This is to circumvent problems with job schedulers that do not configure the host file with requested ppn number of cores.  (See comment in the script).

## pbs scripts

The pbs scripts follow standard format.  The common practice is to isolate the user configurable parameters at the beginning of the scripts, e.g. module commands, binary directory, output directory, etc.  The following scripts have been provided.  Each script is set to run multiple parameter sets, each set in triplicate.

```
a2a_simple_weak.pbs
```
This script tests weak scaling performance of Alltoall and the implementation based on point-to-point.  This script varies the message size in order to activate the different MPI Alltoall algorithms. 


```
a2a_simple_strong.pbs
alltoallv.pbs
```
These scripts benchmarks MPI Alltoall and Alltoallv implementations as well as our implementation that uses point-to-point communication.  

```
mem.pbs
```
This script uses the "benchmark_mem" binary to test memory scaling within a node. It is OpenMP based, and tests different number of cores and with different core binding strategies.

# Analysis of data

The log files generated contains 1 or more blocks of timing and memory usage results that resembles the data shown below.

```
[TIME] R 0/1024
[TIME] benchmark	header (s)	[,mpi_init,barrier_alloc,alloc,barrier_alltoall,alltoall,barrier_alltoall_inplace,alltoall_inplace,barrier_mxx::alltoall,mxx::alltoall,barrier_vec alloc,vec alloc,
barrier_vec_alltoall,vec_alltoall,]
[TIME] benchmark	dur_min	[,0.556292953,0.000554268,0.000004636,0.000013146,0.274134450,0.000026627,0.251003863,0.000031965,0.244533709,0.000033942,0.000001544,0.000013442,0.249684831,]
[TIME] benchmark	dur_max	[,0.704181826,0.023505530,0.000019162,0.021888099,0.403593643,0.129668812,0.367366293,0.117431964,0.378284682,0.135403397,0.000017667,0.018318045,0.390232696,]
[TIME] benchmark	dur_mean	[,0.604079934,0.011712569,0.000006614,0.018568886,0.353050998,0.051055171,0.320635882,0.046648877,0.330915630,0.047919439,0.000006669,0.010850965,0.340534670,]
[TIME] benchmark	dur_stdev	[,0.034459455,0.005150991,0.000001129,0.004783515,0.022798744,0.022793791,0.020556176,0.020595008,0.022626271,0.022682967,0.000001150,0.004310862,0.024189962,]
[TIME] benchmark	cum_min2	[,0.556576518,0.565205377,0.565218149,0.586535067,0.865221618,0.991251177,1.246866807,1.358281143,1.613935752,1.737201117,1.737208583,1.745532542,2.010049932,]
[TIME] benchmark	cum_max2	[,0.707173133,0.716550087,0.723088774,0.737878011,1.110755538,1.142138203,1.480841612,1.509583696,1.859001002,1.888477639,1.888485339,1.896880604,2.259365632,]
[TIME] benchmark	cum_mean2	[,0.604705466,0.616456903,0.616525470,0.635113893,0.988166156,1.039254123,1.359891754,1.406571926,1.737489232,1.785439221,1.785447350,1.796315284,2.136851144,]
[TIME] benchmark	cum_stdev2	[,0.034495212,0.034806829,0.034850310,0.034519160,0.041217516,0.034491005,0.040210973,0.034518270,0.041214968,0.034520955,0.034520995,0.034758741,0.041586801,]
[TIME] benchmark	cnt_min	[,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,]
[TIME] benchmark	cnt_max	[,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,]
[TIME] benchmark	cnt_mean	[,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,0.00,1024.00,]
[TIME] benchmark	cnt_stdev	[,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,]
[MEM] R 0/1024
[MEM] benchmark	header (MB)	[,begin,mpi_init,alloc,alltoall,alltoall_inplace,mxx::alltoall,vec alloc,vec_alltoall,]
[MEM] benchmark	curr_min	[,2.406,20.984,21.012,22.348,22.582,22.582,22.594,22.594,]
[MEM] benchmark	curr_max	[,2.410,21.438,22.676,25.492,25.727,26.250,26.250,26.625,]
[MEM] benchmark	curr_mean	[,2.406,21.380,21.416,22.426,22.865,22.871,22.905,22.913,]
[MEM] benchmark	curr_stdev	[,0.000,0.099,0.190,0.238,0.287,0.304,0.346,0.363,]
[MEM] benchmark	peak_min	[,2.414,21.160,21.176,30.352,30.789,30.789,30.789,30.797,]
[MEM] benchmark	peak_max	[,2.418,75.613,75.613,75.613,75.613,75.613,75.613,75.613,]
[MEM] benchmark	peak_mean	[,2.414,24.795,24.829,33.255,33.681,33.687,33.687,33.726,]
[MEM] benchmark	peak_stdev	[,0.000,13.120,13.112,10.938,10.829,10.828,10.828,10.820,]

```

Each block like this correspond to the time and memory usage measurement for a major block of code, corresponding to a function or a portion of a function.  Each column within a block corresponds to a major step in the code block.

Each row shows a statistical summary of a particular measurement across all MPI processes, in this case, 1024 processes.  "Dur" is duration, "cum" is total time since start of execution, "cnt" may indicate the number of elements processed per MPI rank or could be some other numeric measure - please see source code for exact meaning.  "curr" is the current memory usage, and "peak" is the peak memory usage so far.

## Data Extraction

The typical workflow for analyzing the results involves extracting the relevant rows of the data and importing into a spreadsheet.  "grep" can be used to extract specific rows. 


Data from multiple log files, each corresponding to a different parameter set, can be imported into a spreadsheet program for plotting the scaling behavior.  Excel is a good spreadsheet for this purpose as its pivot table feature allows exploration of the parameter space easily, and it can also average or take the extrema of the measurements across multiple runs.



To identify which row is relevant, the name of the block (e.g. "benchmark") can be used as search term in the source code.  Below the major blocks are described for each of the executables. 

### benchmark_mem

The aim of this benchmark is to test the memory access scaling, as well as NUMA effects on the system.

Since this is an OpenMP executable, the results are actually simpler - there is only one single measurement for each of "dur", "cum", "cnt", "curr", and "peak".  All measurements are collected in a single block.  The columns are roughly in 3 blocks:  "central", "local", and "global".

"central" refers to the master thread allocating a single array that all threads access.  The array is partitioned and each thread only operates on its own partition.  Linear and random access patterns are tested, each, again is limited to its own partition.

"local" refers to each thread having its own thread local array.

"global" refers to the case where the master thread allocating a single array, but threads can access the entire array and is not limited to its own partition.

### benchmark_a2av
The aim of this benchmark is to test the performance of MPI Alltoallv, through the mxx wrapper, and in the presence of simulated computation.

The primary blcok is "benchmark".   The columns are organized into several groups: "mxx::a2av", "a2av_isend", "a2av_isend2".

"mxx::a2av" uses mxx, which in this case basically wraps the MPI_Alltoallv function call.

"a2av_isend" is an reimplementation of the MPI_Alltoallv algorithm using non-blocking send and receive.  The objective is to interleave the (simulated) computation.

"a2av_isend2" is another reimplementation, similar to "a2av_isend", except that the rank ordering for the send-receive pairs is such that all ranks communicate with intra node ranks first before inter-node communication.  (normal communication can have some ranks communicate within a node while some other ranks from the same node communicate with other nodes, thus creating communication time imbalance.)

### benchmark_a2a_simple

This benchmark just calls MPI_Alltoall with some special cases.  The data size is hardcoded to 1 unsigned long, because this is a frequent use case.  THIS EXECUTABLE SHOULD NOT BE USED AT THIS POINT.

"alltoall" is a normal MPI_Alltoall.  Note that depending on pair-wise message size, 4 different algorithms could be used.
To avoid changing algorithm during a processor count scaling run, pair-wise message size is a constant during scaling runs.  however, this also means that per-comm-iteration message size scales with P.  This should be kept in mind during analysis, and possibly compensated.

Doubling P doubles the number of communication iterations for most algorithms.  Each iterations should require \tau + m \mu, as each processor-pair communication ideally proceed in parallel.  In reality, there will be contentions on wire and within nodes, possibly serializing amongst c cores in a node, thus multiplying by a c^2/2 factor -> c^2/2 ( \tau + m \mu ), over p rounds.

Translating this to constant-per-core weak scaling, scale time by 1/p.  Note that this should demonstrate the quadratic scaling with core count.

"alltoall_inplace" uses the in-place version of MPI_Alltoall.

