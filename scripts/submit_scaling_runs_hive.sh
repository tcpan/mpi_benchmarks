#!/bin/sh

if [ $# -gt 0 ]
then
	echo "input1: $1"

	if [ $# -gt 1 ]
	then
		# previous job id supplied.

		echo "input2: $2"
		one=`echo $2 | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`
		echo "parsed job id $one"

		two=$(qsub -l nodes=128:ppn=24 -W depend=afterany:$one $1)
		one=`echo $two | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`

		echo "jobid from warmup 1: $one"
	else
		# single parameter - no previous run id supplied.

		two=$(qsub -l nodes=128:ppn=24 $1)
		one=`echo $two | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`

		echo "jobid from warmup 2: $one"
	fi


	# submit for each NODE count
	for j in 64 32 16 8 4 2 1  #64
	do

		# DONT submit per core count.  Let the PBS scripts handle that.
		# RATIONALE: the hostfile from SWARM's job scheduler does not support this mode of operation
		# it contains nodes*ppn number of entries in groups of 28 identical nodes,
		# regardless of what the specified ppns are.  Also, -n switch does not work.
		#for i in 1 2 3 4 7 8 14 16 28
		#do

			# -n does not work.
			
			two=$(qsub -l nodes=${j}:ppn=24 -W depend=afterany:$one $1)
			one=`echo $two | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`
			
			echo $one
		#done

	done

else

	echo "USAGE: $0 pbs_script [existing_job_id]"
fi
