#!/bin/sh

if [ $# -gt 0 ]
then

	if [ $# -gt 1 ]
	then
		# previous job id supplied.

		echo "input2: $2"
		one=`echo $2 | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`
		echo "parsed job id $one"

		two=$(sbatch --nodes=1 --tasks-per-node=48 --cpus-per-task=1 --dependency=afterany:$one $1)
		one=`echo $two | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`

	else
		# single parameter - no previous run id supplied.

		two=$(sbatch --nodes=1 --tasks-per-node=48 --cpus-per-task=1 $1)
		one=`echo $two | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`

	fi
	echo $one


	# submit for each CORE count
	for j in 2 4 8 16 32 64
	do

		# DONT submit per core count.  Let the PBS scripts handle that.
		# RATIONALE: the hostfile from SWARM's job scheduler does not support this mode of operation
		# it contains nodes*ppn number of entries in groups of 28 identical nodes,
		# regardless of what the specified ppns are.  Also, -n switch does not work.
		#for i in 1 2 3 4 7 8 14 16 28
		#do

			# -n does not work.
			
			two=$(sbatch --nodes=$j --tasks-per-node=48 --cpus-per-task=1 --dependency=afterany:$one $1)
			one=`echo $two | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`
			
			echo $one
		#done

	done

else

	echo "USAGE: $0 pbs_script [existing_job_id]"
fi
